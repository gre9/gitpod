FROM alpine:latest

RUN apk add bash bash-completion bind-tools curl docker docker-vim gawk httpie git git-bash-completion go goaccess gzip jq fzf links lynx mc mtr openssl python3 py3-pip py3-pip-bash-completion ruby ripgrep ripgrep-bash-completion sed tar tcptraceroute tig tmux vim
